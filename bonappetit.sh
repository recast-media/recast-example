#!/usr/bin/env bash
case $1 in
	cast)
		if [[ ! -e "$HOME/.cache/bonappetit.rcc" ]]; then
			curl -o "$HOME/.cache/bonappetit.rcc" "https://gitlab.com/recast-media/recast-example/-/raw/master/bonappetit.rcc?inline=false"
		fi
		echo -n payload | websocat ws://localhost:20020
		cat "$HOME/.cache/bonappetit.rc" | websocat --binary ws://localhost:20020
		echo -n payload_done | websocat ws://localhost:20020
		;;
	play)
		echo -n play | websocat ws://localhost:20020
		;;
	pause)
		echo -n pause | websocat ws://localhost:20020
		;;
	next)
		echo -n next | websocat ws://localhost:20020
		;;
	previous)
		echo -n previous | websocat ws://localhost:20020
		;;
	seek)
		echo -n jumpToTime $2 | websocat ws://localhost:20020
		;;
	stop)
		echo -n reset | websocat ws://localhost:20020
		;;
	*)
		echo "$0 — let recast die for claire from the bon appetit test kitchen"
		echo "	cast			start casting to a receiver running on localhost"
		echo "	stop			stop casting"
		echo "	play			press play"
		echo "	pause			press pause"
		echo "	next			press next"
		echo "	previous		press previous"
		echo "	seek <time>		seek to <time>"
		;;
esac
